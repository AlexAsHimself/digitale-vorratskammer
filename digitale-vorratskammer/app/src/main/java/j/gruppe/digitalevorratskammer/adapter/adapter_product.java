package j.gruppe.digitalevorratskammer.adapter;

/**
 * @author Alexander Henseler
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import j.gruppe.digitalevorratskammer.R;
import j.gruppe.digitalevorratskammer.activitys.MainActivity;
import j.gruppe.digitalevorratskammer.datamodels.produkt;


public class adapter_product  extends ArrayAdapter<produkt> {

    public adapter_product(Context context, ArrayList<produkt> users) {
        super(context, 0, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        produkt prd = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_products, parent, false);
        }

        TextView tv_name = convertView.findViewById(R.id.item_title_product_list);
        TextView tv_einheit = convertView.findViewById(R.id.item_volumeUnit_product_list);
        TextView tv_id = convertView.findViewById(R.id.id_product_item);
        ImageView iv_icon = convertView.findViewById(R.id.item_categoryIcon_product_list);

        switch (prd.getKategoie_id()){
            case 2:
                iv_icon.setImageResource(R.drawable.ic_rice);
                break;
            case 3:
                iv_icon.setImageResource(R.drawable.ic_obst);
                break;
            case 4:
                iv_icon.setImageResource(R.drawable.ic_gemuese);
                break;
            case 5:
                iv_icon.setImageResource(R.drawable.ic_fleisch);
                break;
            case 6:
                iv_icon.setImageResource(R.drawable.ic_suess);
                break;
            case 7:
                iv_icon.setImageResource(R.drawable.ic_drink);
                break;
            case 8:
                iv_icon.setImageResource(R.drawable.ic_shampoo);
                break;
            default:
                iv_icon.setImageResource(R.drawable.ic_sonstiges);
        }

        tv_name.setText(prd.getName());
        tv_einheit.setText(MainActivity.h_einheit.getNameById(prd.getEinheit_id()));
        tv_id.setText(String.valueOf(prd.getId()));

        return convertView;
    }
}
