package j.gruppe.digitalevorratskammer.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import j.gruppe.digitalevorratskammer.datamodels.einheit;
import j.gruppe.digitalevorratskammer.datamodels.kategorie;
import j.gruppe.digitalevorratskammer.datamodels.listeneintrag;
import j.gruppe.digitalevorratskammer.datamodels.produkt;
import j.gruppe.digitalevorratskammer.datamodels.rezept;
import j.gruppe.digitalevorratskammer.datamodels.vorrat;
import j.gruppe.digitalevorratskammer.datamodels.zutat;

//Import der eigenen Packages

/**
 * @author Alexander Henseler
 */

public class sql_datasource{

    private SQLiteDatabase database;
    private sql_helper dbHelper;

    public sql_datasource(Context context) {
        dbHelper = new sql_helper(context);
    }


    /////////////////////////////////////////////////////////////////////////////
    //      Methoden für PRODUKT
    ////////////////////////////////////////////////////////////////////////////

    //ArrayList mit Objekten von allen Produkten zurückgeben
    public ArrayList<produkt> getAllProducts(){
        ArrayList<produkt> ar_produkte = new ArrayList<>();
        open();
        Cursor cur = database.rawQuery("SELECT * FROM produkt;", null);
        while(cur.moveToNext()){
            int id = cur.getInt(cur.getColumnIndex("id"));
            String name = cur.getString(cur.getColumnIndex("name"));
            long barcode = cur.getLong(cur.getColumnIndex("barcode"));
            int kategorie_id = cur.getInt(cur.getColumnIndex("kategorie_id"));
            int einheit_id = cur.getInt(cur.getColumnIndex("einheit_id"));
            ar_produkte.add(new produkt(id, name, barcode, kategorie_id, einheit_id));
        }
        close();
        return ar_produkte;
    }

    //Produkt zur Datenbank hinzufügen und ID zurück geben
    public int addProdukt(String _name, long _barcode, int _kategorie, int _einheit){
        open();
        ContentValues values = new ContentValues();
        values.put("name", _name);
        if (_barcode != 0)
            values.put("barcode", _barcode);
        values.put("kategorie_id", _kategorie);
        values.put("einheit_id", _einheit);
        Log.d("LUL -- 3", "addProdukt: ");
        int id = (int) database.insert("produkt", null, values);
        close();
        return id;
    }

    public void updateProdukt(int _id, String _name, long _barcode, int _kategorie, int _einheit){
        open();
        ContentValues cv = new ContentValues();
        cv.put("name", _name);
        cv.put("barcode", _barcode);
        cv.put("kategorie_id", _kategorie);
        cv.put("einheit_id", _einheit);
        database.update("produkt", cv, "id=" + String.valueOf(_id), null);
        close();
    }


    public void deleteProduct(int id){
        open();
        database.delete("produkt", "id" + "=" + id, null);
        database.delete("listeneintrag", "produkt_id" + "=" + id, null);
        database.delete("vorrat", "produkt_id" + "=" + id, null);
        database.delete("zutat", "produkt_id" + "=" + id, null);
        close();
    }


    /////////////////////////////////////////////////////////////////////////////
    //      Methoden für KATEGORIE
    ////////////////////////////////////////////////////////////////////////////

    public ArrayList<kategorie> getAllCategories(){
        ArrayList<kategorie> ar_kategorie = new ArrayList<>();
        open();
        Cursor cur = database.rawQuery("SELECT * FROM kategorie;", null);
        while(cur.moveToNext()){
            int id = cur.getInt(cur.getColumnIndex("id"));
            String name = cur.getString(cur.getColumnIndex("name"));
            ar_kategorie.add(new kategorie(id, name));
        }
        close();
        return ar_kategorie;
    }

    public int addCategorie(String _name){
        open();
        ContentValues values = new ContentValues();
        values.put("name", _name);
        int id = (int) database.insert("kategorie", null, values);
        close();
        return id;
    }


    /////////////////////////////////////////////////////////////////////////////
    //      Methoden für Listeneinträge
    ////////////////////////////////////////////////////////////////////////////
    public ArrayList<listeneintrag> getAllListEntrys(){
        ArrayList<listeneintrag> ar_listeneintrag = new ArrayList<>();
        open();
        Cursor cur = database.rawQuery("SELECT * FROM listeneintrag;", null);
        while(cur.moveToNext()){
            int id = cur.getInt(cur.getColumnIndex("id"));
            int produkt_id = cur.getInt(cur.getColumnIndex("produkt_id"));
            int menge = cur.getInt(cur.getColumnIndex("menge"));
            int list_id = cur.getInt(cur.getColumnIndex("liste_id"));
            int is_checked = cur.getInt(cur.getColumnIndex("is_checked"));
            ar_listeneintrag.add(new listeneintrag(id, produkt_id, list_id, menge, is_checked));
        }
        close();
        return ar_listeneintrag;
    }

    public int addListEntry(int _produkt_id, int _list_id, int _menge){
        open();
        ContentValues values = new ContentValues();
        values.put("liste_id", _list_id);
        values.put("produkt_id", _produkt_id);
        values.put("menge", _menge);
        values.put("is_checked", 0);
        int id = (int) database.insert("listeneintrag", null, values);
        close();
        return id;
    }

    public void checkListeneintrag(int _id, int _is_checked){
        open();
        ContentValues values = new ContentValues();
        values.put("is_checked", _is_checked);
        database.update("listeneintrag", values, "id=" + String.valueOf(_id), null);
        close();
    }

    public void deleteListEntry(int _produkt_id, int _list_id){
        open();
        String where_clause = "produkt_id=" + _produkt_id + " AND " + "liste_id=" + _list_id;
        database.delete("listeneintrag", where_clause, null);
        close();
    }

    public void updateListEntry(int _produkt_id, int _list_id, int _menge){
        open();
        ContentValues values = new ContentValues();
        values.put("menge", _menge);
        String where_clause = "produkt_id=" + _produkt_id + " AND " + "liste_id=" + _list_id;
        database.update("listeneintrag", values, where_clause, null);
        close();
    }




    /////////////////////////////////////////////////////////////////////////////
    //      Methoden für Einheit
    ////////////////////////////////////////////////////////////////////////////
    public ArrayList<einheit> getUnits(){
        ArrayList<einheit> ar_einheit = new ArrayList<>();
        open();
        Cursor cur = database.rawQuery("SELECT * FROM einheit;", null);
        while(cur.moveToNext()){
            int id = cur.getInt(cur.getColumnIndex("id"));
            String name = cur.getString(cur.getColumnIndex("name"));
            ar_einheit.add(new einheit(id,name));
        }
        close();
        return ar_einheit;
    }

    /////////////////////////////////////////////////////////////////////////////
    //      Methoden für Vorräte
    ////////////////////////////////////////////////////////////////////////////
    public ArrayList<vorrat> getVorrat(){
        ArrayList<vorrat> ar_vorrat = new ArrayList<>();
        open();
        Cursor cur = database.rawQuery("SELECT * FROM vorrat;", null);
        while(cur.moveToNext()){
            int id = cur.getInt(cur.getColumnIndex("id"));
            int produkt_id = cur.getInt(cur.getColumnIndex("produkt_id"));
            int menge = cur.getInt(cur.getColumnIndex("menge"));
            ar_vorrat.add(new vorrat(id, produkt_id, menge));
        }
        close();
        return ar_vorrat;
    }

    public int addVorrat(int _produkt_id, int _menge){
        open();
        ContentValues values = new ContentValues();
        values.put("produkt_id", _produkt_id);
        values.put("menge", _menge);
        int id = (int) database.insert("vorrat", null, values);
        close();
        return id;
    }

    public void updateVorrat(int _produkt_id, int _menge){
        open();
        ContentValues values = new ContentValues();
        values.put("menge", _menge);
        database.update("vorrat", values, "produkt_id=" + _produkt_id, null);
        close();
    }

    public void deleteVorrat(int _produkt_id){
        open();
        database.delete("vorrat", "produkt_id" + "=" + _produkt_id, null);
        close();
    }

    /////////////////////////////////////////////////////////////////////////////
    //      Methoden für Rezepte
    ////////////////////////////////////////////////////////////////////////////
    public ArrayList<rezept> getAllRecipes(){
        ArrayList<rezept> ar_rezept = new ArrayList<>();
        open();
        Cursor cur = database.rawQuery("SELECT * FROM rezept;", null);
        while(cur.moveToNext()){
            int id = cur.getInt(cur.getColumnIndex("id"));
            String name = cur.getString(cur.getColumnIndex("name"));
            int personen = cur.getInt(cur.getColumnIndex("personen"));
            int favorit = cur.getInt(cur.getColumnIndex("favorite"));
            String bild = cur.getString(cur.getColumnIndex("bild"));
            ar_rezept.add(new rezept(id, name, personen, favorit, bild));
        }
        close();
        return ar_rezept;
    }

    public int addRezept(String _name, int _personen, int _favorit, String _bild){
        open();
        ContentValues values = new ContentValues();
        values.put("name", _name);
        values.put("personen", _personen);
        values.put("favorite", _favorit);
        values.put("bild", _bild);
        int id = (int) database.insert("rezept", null, values);
        close();
        return id;
    }

    public void deleteRezept(int _id){
        open();
        database.delete("rezept", "id" + "=" + _id, null);
        close();
    }

    /////////////////////////////////////////////////////////////////////////////
    //      Methoden für Zutaten
    ////////////////////////////////////////////////////////////////////////////
    public ArrayList<zutat> getZutaten(int _rezept_id){
        ArrayList<zutat> ar_zutaten = new ArrayList<>();
        open();
        Cursor cur = database.rawQuery("SELECT * FROM zutat WHERE rezept_id=" + _rezept_id + ";" , null);
        while(cur.moveToNext()){
            int id = cur.getInt(cur.getColumnIndex("id"));
            int produkt_id = cur.getInt(cur.getColumnIndex("produkt_id"));
            int rezept_id = cur.getInt(cur.getColumnIndex("rezept_id"));
            int menge = cur.getInt(cur.getColumnIndex("menge"));
            String einheit = cur.getString(cur.getColumnIndex("einheit"));
            ar_zutaten.add(new zutat(id, rezept_id, produkt_id, menge, einheit));
        }
        close();
        return ar_zutaten;
    }

    public int addZutat(int _rezept_id, int _produkt_id, int _menge, String _einheit){
        open();
        ContentValues values = new ContentValues();
        values.put("rezept_id", _rezept_id);
        values.put("produkt_id", _produkt_id);
        values.put("menge", _menge);
        values.put("einheit", _einheit);
        int id = (int) database.insert("zutat", null, values);
        close();
        return id;
    }

    public void deleteZutatForRecipe(int _id){
        open();
        database.delete("zutat", "rezept_id" + "=" + _id, null);
        close();
    }


    /**
     * öffnet Datenback
     */
    public void open(){
        database = dbHelper.getWritableDatabase();
    }

    /**
     * schließt Datenback
     */
    public void close(){
        dbHelper.close();
    }
}
