package j.gruppe.digitalevorratskammer.adapter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import j.gruppe.digitalevorratskammer.activitys.MainActivity;
import j.gruppe.digitalevorratskammer.datamodels.listeneintrag;
import j.gruppe.digitalevorratskammer.datamodels.rezept;
import j.gruppe.digitalevorratskammer.datamodels.vorrat;
import j.gruppe.digitalevorratskammer.datamodels.produkt;
import j.gruppe.digitalevorratskammer.datamodels.zutat;

/**
 * @author Leon Rozmarynowski
 */
public class combine_and_sort {

    private static ArrayList<vorrat> ar_Vorrat = new ArrayList<>();
    private static ArrayList<listeneintrag> ar_listeneintrag_einkauf = new ArrayList<>();
    private static ArrayList<listeneintrag> ar_listeneintrag_wunsch = new ArrayList<>();
    private static ArrayList<produkt> ar_products = new ArrayList<>();
    private static ArrayList<rezept> ar_rez = new ArrayList<>();
    public static ArrayList<rezept> ar_rez_kochbar = new ArrayList<>();

    public combine_and_sort() {
        ar_Vorrat = MainActivity.h_vorrat.ar_vorrat;
        ar_listeneintrag_einkauf = MainActivity.h_listeneintrag.ar_listeneintrag_einkauf;
        ar_listeneintrag_wunsch = MainActivity.h_listeneintrag.ar_listeneintrag_wunsch;
        ar_products = MainActivity.h_produkt.ar_produkte;
        ar_rez = MainActivity.h_rezept.ar_rezept;
    }


    public void combine() {
        ArrayList<listeneintrag> ar_listeneintrag_rest = new ArrayList<>();

        /**
         * Vergleiche Wunschlist mit Vorrat
         * erstelle "neue" Wunschlist - Vorratslist
         * (1) gehe jeden Wunscheintrag durch
         * (1.2) gehe jeden Vorrat eintrag durch
         * (2) Vergleiche ob gleiches Produkt
         * (3) Wenn ja, Verglleiche Menge
         * (4) Wenn mehr gewünscht als verfügbar add eintrag mit wunschmenge - Vorratsmenge
         */
        boolean eingetragen;

        for (listeneintrag l : ar_listeneintrag_wunsch) {
            eingetragen = false;
            for (vorrat v : ar_Vorrat) {
                if (l.getProdukt_id() == v.getProdukt_id()) {
                    eingetragen = true;
                    if (l.getMenge() > v.getMenge()) {
                        listeneintrag s = new listeneintrag(l.getId(), l.getProdukt_id(), 0, (l.getMenge() - v.getMenge()), 0);
                        ar_listeneintrag_rest.add(s);
                    }
                }
            }
            if (eingetragen == false) {
                listeneintrag s = new listeneintrag(l.getId(), l.getProdukt_id(), 0, l.getMenge(), 0);
                ar_listeneintrag_rest.add(s);
            }
        }


        /**
         * Vergleiche restliche Wunschlist mit Voratsliste
         * ergänze Einkaufslist mit geänderter Wunschlist
         * (1) gehe jeden Wunscheintrag durch
         * (1.2) gehe jeden Einkaufseintrag durch
         * (2) Vergleiche ob gleiches Produkt
         * (3) Wenn ja, Verglleiche Menge
         * (4) Wenn mehr gewünscht als eingekauft werden soll modifizier eintrag mit wunschmenge über handler_listeneintrag
         * (3.2) wenn nein, dann adde Produkt zu Einkaufsliste über handler_listeneintrag
         */

        for (listeneintrag r : ar_listeneintrag_rest) {
            eingetragen = false;
            for (listeneintrag e : ar_listeneintrag_einkauf) {
                if (r.getProdukt_id() == e.getProdukt_id()) {
                    eingetragen = true;
                    if (r.getMenge() > e.getMenge()) {
                        MainActivity.h_listeneintrag.setEntryAmount(e.getProdukt_id(), r.getMenge(), 1);
                    }
                }
            }
            if (eingetragen == false) {
                MainActivity.h_listeneintrag.addListEntry(r.getProdukt_id(), 1, r.getMenge());
            }
        }

    }

    /**
     * sort der Listen:
     * Listmöglichkeiten: Vorrat, Einkauf, Wunsch, Produkt;
     * Sortierarten:
     * 1 = symbol/Kategorie: Getränk, Essen, Pflege, Sonstiges;
     * 2 = Name
     * 3 = Menge
     * 4 = checked (Einkauf?)
     * 5 = Einheit ( nur produkte?) nach welcher mengenvariante sortieren?
     */

    public void sort_list(String List, final int sortmethod) {
        switch (List) {
            case "Vorrat":

                Collections.sort(ar_Vorrat, new Comparator<vorrat>() {
                    @Override
                    public int compare(vorrat v1, vorrat v2) {
                        int verg = 0;
                        switch (sortmethod) {
                            case 1:
                                verg = Integer.compare(MainActivity.h_produkt.getCategoryById(v1.getProdukt_id()), MainActivity.h_produkt.getCategoryById(v2.getProdukt_id()));
                                break;
                            case 2:
                                verg = MainActivity.h_produkt.getNameById(v1.getProdukt_id()).compareTo(MainActivity.h_produkt.getNameById(v2.getProdukt_id()));
                                break;
                            case 3:
                                verg = Integer.compare(v1.getMenge(), v2.getMenge());
                                break;
                            default:
                                break;
                        }
                        return verg;
                    }
                });
                break;

            case "Einkauf":
                Collections.sort(ar_listeneintrag_einkauf, new Comparator<listeneintrag>() {
                    @Override
                    public int compare(listeneintrag v1, listeneintrag v2) {
                        int verg = 0;
                        switch (sortmethod) {
                            case 2:
                                verg = MainActivity.h_produkt.getNameById(v1.getProdukt_id()).compareTo(MainActivity.h_produkt.getNameById(v2.getProdukt_id()));
                                break;
                            case 3:
                                verg = Integer.compare(v1.getMenge(), v2.getMenge());
                                break;
                            case 4:
                                verg = Boolean.compare(v1.getIs_checked(), v2.getIs_checked());
                                break;
                            default:
                                break;
                        }
                        return verg;
                    }
                });

            case "Wunsch":
                Collections.sort(ar_listeneintrag_wunsch, new Comparator<listeneintrag>() {
                    @Override
                    public int compare(listeneintrag v1, listeneintrag v2) {
                        int verg = 0;
                        switch (sortmethod) {
                            case 1:
                                verg = Integer.compare(MainActivity.h_produkt.getCategoryById(v1.getProdukt_id()), MainActivity.h_produkt.getCategoryById(v2.getProdukt_id()));
                                break;
                            case 2:
                                verg = MainActivity.h_produkt.getNameById(v1.getProdukt_id()).compareTo(MainActivity.h_produkt.getNameById(v2.getProdukt_id()));
                                break;
                            case 3:
                                verg = Integer.compare(v1.getMenge(), v2.getMenge());
                                break;
                            default:
                                break;
                        }
                        return verg;
                    }
                });
                break;

            case "Produkt":
                Collections.sort(ar_products, new Comparator<produkt>() {
                    @Override
                    public int compare(produkt p1, produkt p2) {
                        int verg = 0;
                        switch (sortmethod) {
                            case 1:
                                verg = Integer.compare(p1.getKategoie_id(), p2.getKategoie_id());
                                break;
                            case 2:
                                verg = p1.getName().compareTo(p2.getName());
                                break;
                            case 5:
                                verg = Integer.compare(p1.getEinheit_id(), p2.getEinheit_id());
                                break;
                            default:
                                break;
                        }
                        return verg;
                    }
                });
                break;
            default:
                break;
        }
    }

    /**
     * sort recipes if they have available ingredients
     * TODO Alex: im datamodel rezept muss wenigstens getter für zutaten arrayList geben, sonst kein vergleichen möglich
     */
    public void filter_recipes() {
        boolean kochbar = false;
        boolean zutat_vorhanden = false;
        /**
         * starte rezeptvergleich
         */
        for (rezept r : ar_rez) {
            kochbar = true;

            /**
             * (1) schaue ob zutaten passen (vergleich mit voratskammer)
             * (2) wenn ja, schau ob menge passt
             * (3) Wenn Ja, mach weiter mit nächster zutat
             * (3.2) Wenn Nein, rezept nicht kochbar
             * (2.2) Wenn Nein, rezept nicht kochbar
             * (4) Wenn alle Zutaten in genügender menge verfügbar, dann rezept kochbar
             */

            Zutat:
            for(zutat m : r.getZutaten() ){
                zutat_vorhanden = false;
                for (vorrat v : ar_Vorrat) {
                    if (m.getProdukt_id() == v.getProdukt_id()) {
                        zutat_vorhanden = true;
                        if (m.getMenge() > v.getMenge()) {
                            kochbar = false;
                            break Zutat;
                        }
                    }
                }
                if(zutat_vorhanden == false){
                    kochbar = false;
                    break;
                }

            }

            if (kochbar == true)
                ar_rez_kochbar.add(r);
        }

    }

}