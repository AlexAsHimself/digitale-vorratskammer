package j.gruppe.digitalevorratskammer.activitys;

/**
 * @author Alexander Henseler
 */

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import java.util.ArrayList;

import j.gruppe.digitalevorratskammer.R;
import j.gruppe.digitalevorratskammer.datamodels.einheit;
import j.gruppe.digitalevorratskammer.datamodels.kategorie;
import j.gruppe.digitalevorratskammer.datamodels.produkt;

public class ProductDetailActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener  {

    EditText txt_productTitle;
    Spinner spinn_productUnity;
    Spinner spinn_productCategory;
    EditText txt_productCode;
    Button bt_save;
    Button bt_delete;
    Button bt_stop;
    ImageButton bt_scan;
    String product_id;
    String barcode_old;
    Boolean edit = false;
    Context context;

    private DrawerLayout mDrawerLayout;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        txt_productTitle = findViewById(R.id.txt_title_product);
        spinn_productUnity = findViewById(R.id.spinn_productUnity);
        spinn_productCategory= findViewById(R.id.spinner_productCategory);
        txt_productCode = findViewById(R.id.txt_code_product);
        bt_save = findViewById(R.id.bt_save_product);
        bt_delete = findViewById(R.id.bt_delete_product);
        bt_scan = findViewById(R.id.bt_scan_productCode);
        bt_stop = findViewById(R.id.bt_stop_product);

        context = this;

        setTitle("Produkte bearbeiten");

        //Toolbar mit Button für Drawer aktivieren
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        mDrawerLayout = findViewById(R.id.drawer_layout);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_add);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        fillSpinner();

        if (getIntent().getExtras() != null){
            edit = true;
            Bundle b = getIntent().getExtras();
            String id_int = b.getString("id");
            product_id = id_int;
            produkt prod = (MainActivity.h_produkt.getObjectById(Integer.parseInt(id_int)));
            txt_productTitle.setText(prod.getName());
            txt_productCode.setText(String.valueOf(prod.getBarcode()));
            barcode_old = String.valueOf(prod.getBarcode());
            spinn_productUnity.setSelection(prod.getEinheit_id() - 1);
            spinn_productCategory.setSelection(prod.getKategoie_id() - 1);
        }


        bt_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ScannerActivity.class);
                startActivityForResult(intent, 2001);
            }
        });

        bt_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        bt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveProduct();
            }
        });

        bt_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteProduct();
            }
        });
    }

    private void deleteProduct(){
        if (product_id != null) {
            Intent intent = new Intent();
            intent.putExtra("id", product_id);
            setResult(2101, intent);
        }
        finish();
    }

    private void saveProduct(){
        String alert_text = null;
        if (!txt_productTitle.getText().toString().equals("")) {
            if ((!txt_productCode.getText().toString().equals("") && !txt_productCode.getText().toString().equals("0"))&& !edit  && MainActivity.h_produkt.checkIfBarcodeExists(txt_productCode.getText().toString()) ){
                alert_text = "Barcode existiert bereits1";
            }
            if (edit && !txt_productCode.getText().toString().equals(barcode_old) && MainActivity.h_produkt.checkIfBarcodeExists(txt_productCode.getText().toString())){
                alert_text = "Barcode existiert bereits2";
            }
            Intent intent = new Intent();
            intent.putExtra("id", product_id);
            intent.putExtra("barcode", txt_productCode.getText().toString());
            intent.putExtra("titel", txt_productTitle.getText().toString());
            intent.putExtra("kategorie", spinn_productCategory.getSelectedItem().toString());
            intent.putExtra("einheit", spinn_productUnity.getSelectedItem().toString());
            setResult(2102, intent);
        }

        else {
            alert_text = "Bitte Titel eingeben";
        }
        if (alert_text == null){
            Log.d("LUL -- 1", "addProdukt: ");
            finish();
        }
        else {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which){
                }
            });
            alertDialogBuilder.setTitle("Achtung!");
            alertDialogBuilder.setMessage(alert_text);
            alertDialogBuilder.show();
        }
    }

    private void fillSpinner(){
        ArrayList<String> array_kategorie =  new ArrayList<String>();
        for (kategorie kat: MainActivity.h_kategorie.ar_kategorie){
            array_kategorie.add(kat.getName());
        }

        ArrayList<String> array_einheit = new ArrayList<String>();
        for (einheit einh: MainActivity.h_einheit.ar_einheit){
            array_einheit.add(einh.getName());
        }

        ArrayAdapter<String> adapter_kategorie = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, array_kategorie);
        ArrayAdapter<String> adapter_einheit = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, array_einheit);

        adapter_kategorie.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter_einheit.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinn_productCategory.setAdapter(adapter_kategorie);
        spinn_productUnity.setAdapter(adapter_einheit);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 2001) {
            if(resultCode == Activity.RESULT_OK){
                String barcode = data.getStringExtra("barcode");
                if (!barcode.equals("Barcode?")) {
                    txt_productCode.setText(barcode);
                }
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_larder) {
            Intent intent = new Intent(this, LarderActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_list) {
            Intent intent = new Intent(this, ShoppingListActivity.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_recipe) {
            Intent intent = new Intent(this, RecipesActivity.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_product){
            Intent intent = new Intent(this, ProductActivity.class);
            startActivity(intent);
            finish();
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
