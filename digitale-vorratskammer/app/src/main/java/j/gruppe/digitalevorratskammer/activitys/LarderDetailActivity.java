package j.gruppe.digitalevorratskammer.activitys;

/**
 * @author Alexander Henseler
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import j.gruppe.digitalevorratskammer.R;

public class LarderDetailActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    EditText txt_titleStock;
    TextView txt_quantityStock;
    Button bt_saveStock;
    Button bt_deletStock;
    private DrawerLayout mDrawerLayout;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_larder_detail);

        txt_titleStock = findViewById(R.id.txt_title_larder);
        txt_quantityStock = findViewById(R.id.txt_product_quantity);

        bt_saveStock = findViewById(R.id.bt_save_larder);
        bt_deletStock = findViewById(R.id.bt_delete_larder);


        bt_saveStock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        bt_deletStock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        setTitle("Vorräte bearbeiten");
        //Toolbar mit Button für Drawer aktivieren
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        mDrawerLayout = findViewById(R.id.drawer_layout);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_add);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_larder) {
            Intent intent = new Intent(this, LarderActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_list) {
            Intent intent = new Intent(this, ShoppingListActivity.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_recipe) {
            Intent intent = new Intent(this, RecipesActivity.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_product) {
            Intent intent = new Intent(this, ProductActivity.class);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
