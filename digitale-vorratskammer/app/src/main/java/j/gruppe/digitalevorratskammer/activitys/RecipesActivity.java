package j.gruppe.digitalevorratskammer.activitys;

/**
 * @author Alexander Henseler
 */

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.Spinner;

import j.gruppe.digitalevorratskammer.R;
import j.gruppe.digitalevorratskammer.adapter.adapter_rezepte;
import j.gruppe.digitalevorratskammer.database.ChefkochApi;
import j.gruppe.digitalevorratskammer.datamodels.rezept;

public class RecipesActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Spinner spinn_sorting;
    private ListView list_recipes;
    private FloatingActionButton float_addRecipe;
    private DrawerLayout mDrawerLayout;
    private adapter_rezepte adp_recipe;
    Context context;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipes);

        spinn_sorting = findViewById(R.id.spinn_recipes);
        list_recipes = findViewById(R.id.list_recipes);
        float_addRecipe = findViewById(R.id.bt_add_recipe);

        context = this;

        float_addRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),RecipesEditActivity.class);
                startActivity(i);
            }
        });

        setTitle("Rezepte");

        //Toolbar mit Button für Drawer aktivieren
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        mDrawerLayout = findViewById(R.id.drawer_layout);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_add);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        adp_recipe = new adapter_rezepte(this, MainActivity.h_rezept.ar_rezept);
        list_recipes.setAdapter(adp_recipe);

        list_recipes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                rezept rzp = (rezept) list_recipes.getItemAtPosition(i);
                view_rezept(rzp);
            }
        });

        list_recipes.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> arg0, View v, int index, long arg3) {
                rezept rzp = (rezept) list_recipes.getItemAtPosition(index);
                edit_rezept(rzp);
                return true;
            }
        });
    }

    private void view_rezept(rezept _rzp){
        Intent intent = new Intent(getApplicationContext(), RecipesDetailActivity.class);
        intent.putExtra("id", _rzp.getId());
        startActivity(intent);
    }


    private void edit_rezept(rezept _rzp){
        Intent intent = new Intent(getApplicationContext(), RecipesEditActivity.class);
        intent.putExtra("id", _rzp.getId());
        startActivityForResult(intent, 6002);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 6002) {
            Log.d("LISTVIEW: ", "Aktualisiert");
            adp_recipe.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_rezept, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.import_recipe:
                importFromApi();
                adp_recipe.notifyDataSetChanged();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void importFromApi(){
        final Dialog d = new Dialog(context);
        d.setTitle("Importieren");
        d.setContentView(R.layout.alert_import);
        Button bt_ok = d.findViewById(R.id.bt_import);
        final EditText et_url =  d.findViewById(R.id.chef_url);
        bt_ok.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String url = et_url.getText().toString();
                callImport(url);
                d.dismiss();
            }
        });
        d.show();
    }

    public void callImport(String _url){
        try {
            String nix = new ChefkochApi().execute(_url).get();
        }
        catch (Exception e){
            Log.d("IMPORT -- 2: ", e.toString());
        }
        adp_recipe.notifyDataSetChanged();
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_larder) {
            Intent intent = new Intent(this, LarderActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_list) {
            Intent intent = new Intent(this, ShoppingListActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_wishlist) {
            Intent intent = new Intent(this, WishListActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_recipe) {
            Intent intent = new Intent(this, RecipesActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_product){
            Intent intent = new Intent(this, ProductActivity.class);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
