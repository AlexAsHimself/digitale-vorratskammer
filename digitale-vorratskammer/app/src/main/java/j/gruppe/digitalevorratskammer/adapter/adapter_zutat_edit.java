package j.gruppe.digitalevorratskammer.adapter;

/**
 * @author Alexander Henseler
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import j.gruppe.digitalevorratskammer.R;
import j.gruppe.digitalevorratskammer.activitys.MainActivity;
import j.gruppe.digitalevorratskammer.datamodels.zutat;

public class adapter_zutat_edit extends ArrayAdapter<zutat> {

    public adapter_zutat_edit(Context context, ArrayList<zutat> users) {
        super(context, 0, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        zutat zut = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_recipesdetail, parent, false);
        }

        TextView tv_name = convertView.findViewById(R.id.item_title_ingredients_recipe);
        TextView tv_menge = convertView.findViewById(R.id.item_quantity_recipe);
        TextView tv_einheit = convertView.findViewById(R.id.item_volumeUnit_recipe);
        TextView tv_id = convertView.findViewById(R.id.id_recipesdetail_item);
        int produktID = zut.getProdukt_id();

        String einheit = MainActivity.h_einheit.getNameById(MainActivity.h_produkt.getUnitById(produktID));
        String name = MainActivity.h_produkt.getNameById(produktID);
        String menge = String.valueOf(zut.getMenge());

        tv_name.setText(name);
        if (!menge.equals("0"))
            tv_menge.setText(menge);
        else
            tv_menge.setText("");
        tv_einheit.setText(einheit);

        tv_id.setText(String.valueOf(zut.getId()));
        return convertView;
    }
}

