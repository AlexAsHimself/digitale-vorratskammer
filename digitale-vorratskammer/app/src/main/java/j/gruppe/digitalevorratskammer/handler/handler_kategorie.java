package j.gruppe.digitalevorratskammer.handler;

/**
 * @author Alexander Henseler
 */

import android.util.Log;

import java.util.ArrayList;

import j.gruppe.digitalevorratskammer.database.sql_datasource;
import j.gruppe.digitalevorratskammer.datamodels.kategorie;

//Import der eigenen Packages

public class handler_kategorie {
    public static ArrayList<kategorie> ar_kategorie = new ArrayList<>();
    public static sql_datasource sql_source;

    public handler_kategorie(sql_datasource _sql_source){
        sql_source = _sql_source;
        fillArrayList();
    }

    private void fillArrayList(){
        ar_kategorie = sql_source.getAllCategories();
    }

    /**
     * Fügt eine Kategorie zu der Liste hinzu:
     *  (1) überprüfen ob  Name bereits vorhanden
     *  (2) Wenn nicht vorhanden zur SQL-Datenbank hinzufügen und ID zurückbekommen
     *  (3) Zur ArrayList hinzufügen
     *  (4) ReturnCode zurück geben
     */
    public int addKategorie(String _name){
        int return_code = 0;
        for (kategorie kat: ar_kategorie){
            if (kat.getName().equals(_name)) {
                return_code = 482;
                break;
            }
        }
        if (return_code == 0) {
            int new_id = sql_source.addCategorie(_name);
            ar_kategorie.add(new kategorie(new_id, _name));
        }
        return return_code;
    }

    public int getIdByName(String name){
        int id = 0;
        for (kategorie kat: ar_kategorie){
            if (kat.getName().equals(name)) {
                id = kat.getId();
            }
        }
        return id;
    }

}
