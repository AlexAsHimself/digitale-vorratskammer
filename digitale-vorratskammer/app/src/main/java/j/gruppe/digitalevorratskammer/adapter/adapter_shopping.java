package j.gruppe.digitalevorratskammer.adapter;

/**
 * @author Alexander Henseler
 */

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import j.gruppe.digitalevorratskammer.R;
import j.gruppe.digitalevorratskammer.activitys.MainActivity;
import j.gruppe.digitalevorratskammer.datamodels.listeneintrag;

public class adapter_shopping extends ArrayAdapter<listeneintrag> {

    public adapter_shopping(Context context, ArrayList<listeneintrag> users) {
        super(context, 0, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        listeneintrag entry = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_shoppinglist, parent, false);
        }

        TextView tv_menge = convertView.findViewById(R.id.txt_quantity_shoppingList);
        TextView tv_einheit = convertView.findViewById(R.id.txt_volumeUnit_shoppinglist);
        TextView tv_name = convertView.findViewById(R.id.item_title_shoppingList);
        CheckBox cb_checked = convertView.findViewById(R.id.item_check_shoppingList);
        TextView tv_id = convertView.findViewById(R.id.id_shoppinglist_item);

        tv_menge.setText(String.valueOf(entry.getMenge()));
        tv_einheit.setText(MainActivity.h_einheit.getNameById(MainActivity.h_produkt.getUnitById(entry.getProdukt_id())));
        tv_name.setText(MainActivity.h_produkt.getNameById(entry.getProdukt_id()));
        tv_id.setText(String.valueOf(entry.getId()));

        if (entry.getIs_checked()) {
            cb_checked.setChecked(true);
            tv_name.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        }
        else if (!entry.getIs_checked()) {
            cb_checked.setChecked(false);
            tv_name.setPaintFlags(1281);
        }
        return  convertView;
    }
}
