package j.gruppe.digitalevorratskammer.handler;

//Import der eigenen Packages

import android.util.Log;

import java.util.ArrayList;

import j.gruppe.digitalevorratskammer.database.sql_datasource;
import j.gruppe.digitalevorratskammer.datamodels.listeneintrag;

/**
 * @author Alexander Henseler
 */
public class handler_listeneintrag {
    public static ArrayList<listeneintrag> ar_listeneintrag = new ArrayList<>();
    public static ArrayList<listeneintrag> ar_listeneintrag_einkauf = new ArrayList<>();
    public static ArrayList<listeneintrag> ar_listeneintrag_wunsch = new ArrayList<>();
    public static sql_datasource sql_source;

    public handler_listeneintrag(sql_datasource _sql_source){
        sql_source = _sql_source;
        fillArrayList();
    }

    private void fillArrayList(){
        ar_listeneintrag = sql_source.getAllListEntrys();
        sortArray();
    }

    /**
     * Fügt ein Produkt zu der Liste hinzu:
     *  (1) überprüfen ob Produkt bereits auf Liste und Menge =/= 0
     *  (2) Wenn nicht vorhanden zur SQL-Datenbank hinzufügen und ID zurückbekommen
     *  (3) Zur ArrayList hinzufügen
     *  (4) ReturnCode zurück geben
     */
    public int addListEntry(int _produkt_id, int _list_id, int _menge){
        int return_code = 0;
        if (_list_id == 1){
            for (listeneintrag listentry: ar_listeneintrag_einkauf){
                if (listentry.getProdukt_id() ==_produkt_id){
                    return_code = 481;
                }
            }
        }
        else if (_list_id == 2) {
            for (listeneintrag listentry : ar_listeneintrag_wunsch) {
                if (listentry.getProdukt_id() == _produkt_id) {
                    return_code = 481;
                }
            }
        }
        if (_menge == 0){
            return_code = 483;
        }
        if (return_code == 0) {
            int new_id = sql_source.addListEntry(_produkt_id, _list_id, _menge);
            ar_listeneintrag.add(new listeneintrag(new_id, _produkt_id, _list_id, _menge, 0));
            sortArray();
        }
        return return_code;
    }

    public void setEntryAmount(int _produkt_id, int _menge, int _list_id){
        if (_menge == 0){
            removeListEntry(_produkt_id, _list_id);
        }
        else {
            if (_list_id == 1){
                listeneintrag entry = null;
                for (listeneintrag listentry: ar_listeneintrag_einkauf){
                    if (listentry.getProdukt_id() ==_produkt_id){
                        entry = listentry;
                    }
                }
                entry.setMenge(_menge);
                sql_source.updateListEntry(_produkt_id, _list_id, _menge);
            }
            else if (_list_id == 2){
                listeneintrag entry = null;
                for (listeneintrag listentry: ar_listeneintrag_wunsch){
                    if (listentry.getProdukt_id() ==_produkt_id){
                        entry = listentry;
                    }
                }
                entry.setMenge(_menge);
                sql_source.updateListEntry(_produkt_id, _list_id, _menge);
            }
        }
    }

    public void checkEntry(int _id){
        listeneintrag entry = null;
        for (listeneintrag listentry: ar_listeneintrag){
            if (listentry.getId() == _id){
                entry = listentry;
            }
        }
        if (entry.getIs_checked()) {
            entry.setIs_checked(0);
            sql_source.checkListeneintrag(_id, 0);
        }
        else {
            entry.setIs_checked(1);
            sql_source.checkListeneintrag(_id, 1);
        }
        sortArray();
    }

    public void clearShoppingList(){
        ArrayList<listeneintrag> remove = new ArrayList<>();
        for (listeneintrag listentry: ar_listeneintrag_einkauf){
            if (listentry.getIs_checked()){
                remove.add(listentry);
            }
        }

        for (listeneintrag listentry: remove){
            ar_listeneintrag.remove(listentry);
            sortArray();
            sql_source.deleteListEntry(listentry.getProdukt_id(), listentry.getList_id());
        }

    }

    public void removeListEntry(int _produkt_id, int _list_id){
        if (_list_id == 2){
            listeneintrag entry = null;
            for (listeneintrag listentry: ar_listeneintrag_wunsch){
                if (listentry.getProdukt_id() ==_produkt_id){
                    entry = listentry;
                }
            }
            ar_listeneintrag.remove(entry);
            ar_listeneintrag_wunsch.remove(entry);
            sql_source.deleteListEntry(_produkt_id, _list_id);
        }
        else if (_list_id == 1){
            listeneintrag entry = null;
            for (listeneintrag listentry: ar_listeneintrag_einkauf){
                if (listentry.getProdukt_id() ==_produkt_id){
                    entry = listentry;
                }
            }
            ar_listeneintrag.remove(entry);
            ar_listeneintrag_einkauf.remove(entry);
            sql_source.deleteListEntry(_produkt_id, _list_id);
        }
    }

    public void sortArray(){
        ar_listeneintrag_einkauf.clear();
        ar_listeneintrag_wunsch.clear();
        for (listeneintrag listentry: ar_listeneintrag){
            if (listentry.getList_id() == 1){
                ar_listeneintrag_einkauf.add(listentry);
            }
            else if (listentry.getList_id() == 2){
                ar_listeneintrag_wunsch.add(listentry);
            }
        }
    }
}

