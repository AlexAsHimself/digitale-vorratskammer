package j.gruppe.digitalevorratskammer.handler;

import android.util.Log;

import java.util.ArrayList;

import j.gruppe.digitalevorratskammer.database.sql_datasource;
import j.gruppe.digitalevorratskammer.datamodels.rezept;

/**
 * @author Alexander Henseler
 */

public class handler_rezept {
    public static ArrayList<rezept> ar_rezept = new ArrayList<>();
    public static sql_datasource sql_source;

    public handler_rezept(sql_datasource _sql_source){
        sql_source = _sql_source;
        fillArrayList();
    }

    private void fillArrayList(){
        ar_rezept = sql_source.getAllRecipes();
        for (rezept rez: ar_rezept){
            Log.d("REZEPZ: ", rez.getName());
        }
    }

    /**
     * Fügt ein Rezept zu der Liste hinzu:
     *  (1) überprüfen ob  Name bereits vorhanden
     *  (2) Wenn nicht vorhanden zur SQL-Datenbank hinzufügen und ID zurückbekommen
     *  (3) Zur ArrayList hinzufügen
     *  (4) ReturnCode zurück geben
     */
    public int addRezept(String _name, int _personen, int _favorit, String _bild){
        int new_id = 0;
        for (rezept rez: ar_rezept){
            if (rez.getName().equals(_name)) {
                new_id = -1;
                break;
            }
        }
        if (new_id != -1) {
            new_id = sql_source.addRezept(_name, _personen, _favorit, _bild);
            ar_rezept.add(new rezept(new_id, _name, _personen, _favorit, _bild));
        }
        return new_id;
    }

    public rezept getObjById(int _id){
        rezept searched_rzp = null;
        for (rezept rez: ar_rezept){
            if (rez.getId() == _id){
                searched_rzp = rez;
            }
        }
        return searched_rzp;
    }

    public void deleteRecipe(int _id){
        rezept delete_rzp = null;
        sql_source.deleteRezept(_id);
        for (rezept rez: ar_rezept){
            if (rez.getId() == _id){
                delete_rzp = rez;
            }
        }
        ar_rezept.remove(delete_rzp);
    }


}
