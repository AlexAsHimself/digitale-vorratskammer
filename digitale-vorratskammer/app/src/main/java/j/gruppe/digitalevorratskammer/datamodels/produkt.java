package j.gruppe.digitalevorratskammer.datamodels;

/**
 * @author Alexander Henseler
 */

public class produkt {
    private int id;
    private String name;
    private long barcode;
    private int kategoie_id;
    private int einheit_id;

    public produkt(int _id, String _name, long _barcode, int _kategorie_id, int _einheit_id){
        setId(_id);
        setName(_name);
        setBarcode(_barcode);
        setKategoie_id(_kategorie_id);
        setEinheit_id(_einheit_id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getBarcode() {
        return barcode;
    }

    public void setBarcode(long barcode) {
        this.barcode = barcode;
    }

    public int getKategoie_id() {
        return kategoie_id;
    }

    public void setKategoie_id(int kategoie_id) {
        this.kategoie_id = kategoie_id;
    }

    public int getEinheit_id() {
        return einheit_id;
    }

    public void setEinheit_id(int einheit_id) {
        this.einheit_id = einheit_id;
    }
}
