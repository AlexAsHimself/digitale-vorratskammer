package j.gruppe.digitalevorratskammer.datamodels;

/**
 * @author Alexander Henseler
 */

public class listeneintrag {
    private int id;
    private int produkt_id;
    private int menge;
    private int list_id;
    private boolean is_checked;

    public listeneintrag(int _id, int _produkt_id, int _list_id, int _menge, int _is_checked) {
        setId(_id);
        setProdukt_id(_produkt_id);
        setMenge(_menge);
        setIs_checked(_is_checked);
        setList_id(_list_id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProdukt_id() {
        return produkt_id;
    }

    public void setProdukt_id(int produkt_id) {
        this.produkt_id = produkt_id;
    }

    public int getMenge() {
        return menge;
    }

    public void setMenge(int menge) {
        this.menge = menge;
    }

    public boolean getIs_checked() {
        return is_checked;
    }

    public void setIs_checked(int is_checked) {
        if (is_checked == 1)
            this.is_checked = true;
        else
            this.is_checked = false;
    }

    public int getList_id() {
        return list_id;
    }

    public void setList_id(int list_id) {
        this.list_id = list_id;
    }
}