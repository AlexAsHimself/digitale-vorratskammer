package j.gruppe.digitalevorratskammer.activitys;

/**
 * @author Alexander Henseler
 */

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import j.gruppe.digitalevorratskammer.R;
import j.gruppe.digitalevorratskammer.adapter.adapter_product;
import j.gruppe.digitalevorratskammer.datamodels.produkt;

public class ProductActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener  {

    Spinner spinn_sorting;
    ListView list_products;
    FloatingActionButton bt_add;

    private adapter_product adp_product;
    private ArrayAdapter<String> adapter;

    private DrawerLayout mDrawerLayout;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        spinn_sorting = findViewById(R.id.spinner_product);
        list_products = findViewById(R.id.list_products);
        bt_add = findViewById(R.id.bt_add_newProduct);

        setTitle("Produkte");

        //Toolbar mit Button für Drawer aktivieren
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        mDrawerLayout = findViewById(R.id.drawer_layout);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_add);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        adp_product = new adapter_product(this, MainActivity.h_produkt.ar_produkte);
        list_products.setAdapter(adp_product);

        fillSortSpinner();

        bt_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),ProductDetailActivity.class);
                startActivityForResult(i, 3001);
            }
        });

        list_products.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> arg0, View v, int index, long arg3) {
                produkt prod = (produkt) list_products.getItemAtPosition(index);
                editOrDelete(prod);
                return true;
            }
        });

        spinn_sorting.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String sort_selection = adapter.getItem(position);
                switch (sort_selection) {
                    case "Name - aufsteigend":
                        MainActivity.combine_sort.sort_list("Produkt", 2);
                        adp_product.notifyDataSetChanged();
                        break;
                    case "Kategorie - aufsteigend":
                        MainActivity.combine_sort.sort_list("Produkt", 1);
                        adp_product.notifyDataSetChanged();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {}

        });
    }

    private void fillSortSpinner(){
        ArrayList<String> sort_types = new ArrayList<String>();
        sort_types.add("Name - aufsteigend");
        sort_types.add("Kategorie - aufsteigend");
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sort_types);
        spinn_sorting.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 3001) {
            if(resultCode == 2102){
                addProduct(data);
                adp_product.notifyDataSetChanged();
            }
        }
        else if (requestCode == 3002){
            if (resultCode == 2101){
                deleteProduct(data.getStringExtra("id"));
            }
            else if(resultCode == 2102){
                updateProduct(data);
                adp_product.notifyDataSetChanged();
            }
        }
    }

    /**
     * ließt die Daten aus dem zurückgegebenen Intent und legt ein neues Produkt an
     */
    public static void addProduct(Intent data){
        //Daten aus Intent lesen
        long barcode = 0;
        String titel = data.getStringExtra("titel");
        String kategorie = data.getStringExtra("kategorie");
        String einheit = data.getStringExtra("einheit");
        try{
            barcode = Long.parseLong(data.getStringExtra("barcode"));}
        catch (Exception e){
            barcode = 0;
        }

        //Kategorie + Einheit ID holen
        int kategorie_id = MainActivity.h_kategorie.getIdByName(kategorie);
        int einheit_id = MainActivity.h_einheit.getIdByName(einheit);

        //Produkt anlegen
        Log.d("LUL -- 2", "addProdukt: ");
        MainActivity.h_produkt.addProdukt(titel, barcode, kategorie_id, einheit_id);
    }

    private void updateProduct(Intent data){
        //Daten aus Intent lesen
        String id = data.getStringExtra("id");
        long barcode = Long.parseLong(data.getStringExtra("barcode"));
        String titel = data.getStringExtra("titel");
        String kategorie = data.getStringExtra("kategorie");
        String einheit = data.getStringExtra("einheit");

        //Kategorie + Einheit ID holen
        int kategorie_id = MainActivity.h_kategorie.getIdByName(kategorie);
        int einheit_id = MainActivity.h_einheit.getIdByName(einheit);

        //Produkt anlegen
        MainActivity.h_produkt.updateProdukt(id, titel, barcode, kategorie_id, einheit_id);
    }



    /**
     * ruft die Lösch-Funktion für ein Produkt mit der Produkt-ID auf
     */
    private void deleteProduct(String id){
        MainActivity.h_produkt.deleteProduct(id);
        adp_product.notifyDataSetChanged();
    }


    private void editOrDelete(final produkt prod){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setPositiveButton("Bearbeiten", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which){
                Intent intent = new Intent(getApplicationContext(), ProductDetailActivity.class);
                intent.putExtra("id", String.valueOf(prod.getId()));
                startActivityForResult(intent, 3002);
            }
        });
        alertDialogBuilder.setNegativeButton("Löschen", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which){
                deleteProduct(String.valueOf(prod.getId()));
            }
        });
        alertDialogBuilder.setTitle("Listeneintrag");
        alertDialogBuilder.setMessage("Was wollen sie machen?");
        alertDialogBuilder.show();
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_larder) {
            Intent intent = new Intent(this, LarderActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_list) {
            Intent intent = new Intent(this, ShoppingListActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_wishlist) {
            Intent intent = new Intent(this, WishListActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_recipe) {
            Intent intent = new Intent(this, RecipesActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_product){
            Intent intent = new Intent(this, ProductActivity.class);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
