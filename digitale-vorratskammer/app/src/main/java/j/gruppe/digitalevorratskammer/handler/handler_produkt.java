package j.gruppe.digitalevorratskammer.handler;

import android.util.Log;

import java.util.ArrayList;

import j.gruppe.digitalevorratskammer.activitys.MainActivity;
import j.gruppe.digitalevorratskammer.database.sql_datasource;
import j.gruppe.digitalevorratskammer.datamodels.listeneintrag;
import j.gruppe.digitalevorratskammer.datamodels.produkt;
import j.gruppe.digitalevorratskammer.datamodels.vorrat;

//Import der eigenen Packages

/**
 * @author Alexander Henseler
 */

public class handler_produkt {
    public static ArrayList<produkt> ar_produkte = new ArrayList<>();
    public static sql_datasource sql_source;

    public handler_produkt(sql_datasource _sql_source){
        sql_source = _sql_source;
        fillArrayList();
    }

    private void fillArrayList(){
        ar_produkte = sql_source.getAllProducts();
        for (produkt prod: ar_produkte){
            Log.d("PRODUKT: ", prod.getName());
        }
    }

    /**
     * Fügt ein Produkt zu der Liste hinzu:
     *  (1) überprüfen ob Barcode oder Name bereits vorhanden
     *  (2) Wenn nicht vorhanden zur SQL-Datenbank hinzufügen und ID zurückbekommen
     *  (3) Zur ArrayList hinzufügen
     *  (4) ReturnCode zurück geben
     */
    public int addProdukt(String _name, long _barcode, int _kategorie_id, int _einheit_id){
        int return_code = 0;
        for (produkt prod: ar_produkte){
            if (prod.getName().equals(_name)) {
                return_code = 482;
                break;
            }
        }
        if (return_code == 0) {
            Log.d("LUL -- 4", "addProdukt: ");
            int new_id = sql_source.addProdukt(_name, _barcode, _kategorie_id, _einheit_id);
            ar_produkte.add(new produkt(new_id, _name, _barcode, _kategorie_id, _einheit_id));
        }
        return return_code;
    }

    public void updateProdukt(String _id, String _name, long _barcode, int _kategorie_id, int _einheit_id){
        produkt old_produkt = null;
        int id = Integer.valueOf(_id);
        for (produkt prod: ar_produkte){
            if (prod.getId() == id){
                old_produkt = prod;
            }
        }
        old_produkt.setName(_name);
        old_produkt.setBarcode(_barcode);
        old_produkt.setKategoie_id(_kategorie_id);
        old_produkt.setEinheit_id(_einheit_id);

        sql_source.updateProdukt(id, _name, _barcode, _kategorie_id, _einheit_id);
    }

    public int getIdByName(String _name){
        int id = 0;
        for (produkt prod: ar_produkte){
            if (prod.getName().equals(_name)){
                id = prod.getId();
            }
        }
        return id;
    }


    public String getNameById(int _id){
        String prod_name = null;
        for (produkt prod: ar_produkte){
            if (prod.getId() == _id){
                prod_name = prod.getName();
            }
        }
        return prod_name;
    }

    public int getCategoryById(int _id){
        int cat_id = 0;
        for (produkt prod: ar_produkte){
            if (prod.getId() == _id){
                cat_id = prod.getKategoie_id();
            }
        }
        return cat_id;
    }

    public int getUnitById(int _id){
        int unit_id = 0;
        for (produkt prod: ar_produkte){
            if (prod.getId() == _id){
                unit_id = prod.getEinheit_id();
            }
        }
        return unit_id;
    }

    public produkt getObjectById(int _id){
        produkt prod_obj = null;
        for (produkt prod: ar_produkte){
            if (prod.getId() == _id){
                prod_obj = prod;
            }
        }
        return prod_obj;
    }

    public int getIdByBarcode(String _barcode){
        long barcode = Long.parseLong(_barcode);
        int id = 0;
        for (produkt prod: ar_produkte){
            if (prod.getBarcode() == barcode){
                id = prod.getId();
            }
        }
        return id;
    }

    public boolean checkIfBarcodeExists(String _barcode){
        long barcode = Long.parseLong(_barcode);
        boolean exists = false;
        for (produkt prod: ar_produkte){
            if (prod.getBarcode() == barcode){
                exists = true;
            }
        }
        return exists;
    }

    public boolean checkIfProductExists(String _name){
        boolean exists = false;
        for (produkt prod: ar_produkte){
            if (prod.getName().equals(_name)){
                exists = true;
            }
        }
        return exists;
    }


    public void deleteProduct(String _id){
        int id = Integer.parseInt(_id);
        produkt prod_delete = null;
        for (produkt prod: ar_produkte) {
            if (prod.getId() == id) {
                prod_delete = prod;
            }
        }

        //Aus Vorräten entfernen
        ArrayList<vorrat> ar_remove_vorrat = new ArrayList<>();
        for (vorrat h: MainActivity.h_vorrat.ar_vorrat){

            if (h.getId() == Integer.valueOf(_id)){
                Log.d("REMOVED: ", "deleteProduct: ");
                ar_remove_vorrat.add(h);
            }
        }
        for (vorrat h: ar_remove_vorrat){
            MainActivity.h_vorrat.ar_vorrat.remove(h);
        }

        //Aus Listeneinträgen entfernen
        ArrayList<listeneintrag> ar_remove_listeneintrag = new ArrayList<>();
        for (listeneintrag h: MainActivity.h_listeneintrag.ar_listeneintrag){

            if (h.getProdukt_id() == Integer.valueOf(_id)){
                Log.d("REMOVED: ", "deleteProduct: ");
                ar_remove_listeneintrag.add(h);
            }
        }
        for (listeneintrag h: ar_remove_listeneintrag){
            MainActivity.h_listeneintrag.ar_listeneintrag.remove(h);
            MainActivity.h_listeneintrag.sortArray();
        }

        sql_source.deleteProduct(prod_delete.getId());
        ar_produkte.remove(prod_delete);
    }



}
