package j.gruppe.digitalevorratskammer.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author Alexander Henseler
 */

public class sql_helper extends SQLiteOpenHelper {
    //Datenkankname definieren
    private static final String DB_NAME = "digitale_vorratskammer.db";

    //Datenbankversion setzen
    private static final int DB_VERSION = 17;

    //Create-Befehlf für Kategorie
    private static final String SQL_CREATE_KATEGORIE = "CREATE TABLE kategorie (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "name TEXT NOT NULL UNIQUE);";

    //Create-Befehl für Produkt
    private static final String SQL_CREATE_PRODUKT = "CREATE TABLE produkt (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "name TEXT NOT NULL UNIQUE," +
            "barcode LONG," +
            "kategorie_id INTEGER," +
            "einheit_id INTEGER," +
            "FOREIGN KEY(kategorie_id) REFERENCES kategorie(id)," +
            "FOREIGN KEY(einheit_id) REFERENCES einheit(id));";

    //Create-Befehl für Liste
    private static final String SQL_CREATE_LISTE = "CREATE TABLE liste (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "name TEXT NOT NULL UNIQUE);";

    //Create-Befehl für Listeneintrag
    private static final String SQL_CREATE_LISTENEINTRAG = "CREATE TABLE listeneintrag (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "liste_id INTEGER," +
            "produkt_id INTEGER," +
            "menge INTEGER," +
            "is_checked INTEGER," +
            "FOREIGN KEY(liste_id) REFERENCES liste(id)," +
            "FOREIGN KEY(produkt_id) REFERENCES produkt(id));";

    //Create-Befehl für Einheit
    private static final String SQL_CREATE_EINHEIT =  "CREATE TABLE einheit (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "name TEXT NOT NULL UNIQUE);";

    //Create-Befehl für Vorrat
    private static final String SQL_CREATE_VORRAT = "CREATE TABLE vorrat (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "produkt_id INTEGER," +
            "menge INTEGER," +
            "FOREIGN KEY(produkt_id) REFERENCES produkt(id));";

    //Create-Befehl für Rezept
    private static final String SQL_CREATE_REZEPT = "CREATE TABLE rezept (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "name TEXT NOT NULL UNIQUE," +
            "personen INTEGER," +
            "favorite INTEGER," +
            "bild TEXT UNIQUE);";

    //Create-Befehl für Zutat
    private static final String SQL_CREATE_ZUTAT = "CREATE TABLE zutat (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "rezept_id INTEGER NOT NULL," +
            "produkt_id INTEGER NOT NULL," +
            "menge INTEGER," +
            "einheit TEXT," +
            "FOREIGN KEY(rezept_id) REFERENCES rezept(id)," +
            "FOREIGN KEY(produkt_id) REFERENCES produkt(id));";

    /**
     * Konstrukt0r
     */
    public sql_helper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    /**
     * Wenn Datenbank erstellt wird diese Befehle ausführen
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_KATEGORIE);
        db.execSQL(SQL_CREATE_EINHEIT);
        db.execSQL(SQL_CREATE_PRODUKT);
        db.execSQL(SQL_CREATE_LISTE);
        db.execSQL(SQL_CREATE_LISTENEINTRAG);
        db.execSQL(SQL_CREATE_VORRAT);
        db.execSQL(SQL_CREATE_REZEPT);
        db.execSQL(SQL_CREATE_ZUTAT);
        addUnits(db);
        addCategory(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE zutat;");
        db.execSQL("DROP TABLE rezept;");
        db.execSQL("DROP TABLE listeneintrag;");
        db.execSQL("DROP TABLE liste;");
        db.execSQL("DROP TABLE vorrat;");
        db.execSQL("DROP TABLE einheit;");
        db.execSQL("DROP TABLE produkt;");
        db.execSQL("DROP TABLE kategorie;");
        onCreate(db);
    }

    public void addUnits(SQLiteDatabase db){
        db.execSQL("INSERT INTO einheit (name) VALUES (' ');");
        db.execSQL("INSERT INTO einheit (name) VALUES ('Stk');");
        db.execSQL("INSERT INTO einheit (name) VALUES ('Pck');");
        db.execSQL("INSERT INTO einheit (name) VALUES ('Kg');");
        db.execSQL("INSERT INTO einheit (name) VALUES ('g');");
        db.execSQL("INSERT INTO einheit (name) VALUES ('l');");
        db.execSQL("INSERT INTO einheit (name) VALUES ('ml');");
        db.execSQL("INSERT INTO einheit (name) VALUES ('Dose');");
    }

    public void addCategory(SQLiteDatabase db){
        db.execSQL("INSERT INTO kategorie (name) VALUES ('Sonstiges');");
        db.execSQL("INSERT INTO kategorie (name) VALUES ('Essen');");
        db.execSQL("INSERT INTO kategorie (name) VALUES ('Obst');");
        db.execSQL("INSERT INTO kategorie (name) VALUES ('Gemüse');");
        db.execSQL("INSERT INTO kategorie (name) VALUES ('Fleisch');");
        db.execSQL("INSERT INTO kategorie (name) VALUES ('Süßigkeit');");
        db.execSQL("INSERT INTO kategorie (name) VALUES ('Getränk');");
        db.execSQL("INSERT INTO kategorie (name) VALUES ('Pflege');");

    }



}