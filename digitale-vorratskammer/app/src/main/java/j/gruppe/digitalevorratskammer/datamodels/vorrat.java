package j.gruppe.digitalevorratskammer.datamodels;

/**
 * @author Alexander Henseler
 */


public class vorrat {
    private int id;
    private int produkt_id;
    private int menge;

    public vorrat(int _id, int _produkt_id, int _menge){
        setId(_id);
        setProdukt_id(_produkt_id);
        setMenge(_menge);;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProdukt_id() {
        return produkt_id;
    }

    public void setProdukt_id(int produkt_id) {
        this.produkt_id = produkt_id;
    }

    public int getMenge() {
        return menge;
    }

    public void setMenge(int menge) {
        this.menge = menge;
    }
}
