package j.gruppe.digitalevorratskammer.handler;

import java.util.ArrayList;

import j.gruppe.digitalevorratskammer.database.sql_datasource;
import j.gruppe.digitalevorratskammer.datamodels.zutat;

/**
 * @author Alexander Henseler
 */

public class handler_zutat {
    public static sql_datasource sql_source;


    public handler_zutat(sql_datasource _sql_source){
        sql_source = _sql_source;
    }

    public ArrayList<zutat> getZutatForRezept(int _rezept_id){
        ArrayList<zutat> zutaten = sql_source.getZutaten(_rezept_id);
        return zutaten;
    }

    public void addZutat(int _rezept_id, int _produkt_id, int _menge, String _einheit){
        sql_source.addZutat(_rezept_id, _produkt_id, _menge, _einheit);
    }

    public void deleteForRecipe(int _id){
        sql_source.deleteZutatForRecipe(_id);
    }


}
