package j.gruppe.digitalevorratskammer.database;

/**
 * Created by Pol Hansen on 20/06/2018.
 */

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;

import j.gruppe.digitalevorratskammer.activitys.MainActivity;
import j.gruppe.digitalevorratskammer.datamodels.zutat;

public class ChefkochApi extends AsyncTask <String, String, String>{

    public ArrayList<zutat> ar_zutaten = new ArrayList<>();
    public ArrayList<String> ar_produkte = new ArrayList<>();
    JSONObject json;
    JSONObject zutaten;

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }

    protected String doInBackground(String... _uri) {
        String uri = _uri[0];
        Log.d("URL: ", uri);
        String[] splittedUri = uri.split("/");
        String apiUri = "http://api.chefkoch.de/v2/recipes/"+splittedUri[4];

        System.out.println("");
        try {
            json = readJsonFromUrl(apiUri);
            // System.out.println(json.toString());
            String rezept_name = json.get("title").toString();
            Log.d("REZEPT_NAME", rezept_name);

            JSONArray ingredients = json.getJSONArray("ingredientGroups");
            // System.out.println(ingredients.toString());
            JSONArray ing = (JSONArray) ingredients.getJSONObject(0).get("ingredients");
            // System.out.println(ing.toString());
            int i = 0;

            while(i < ing.length()) {
                zutaten = ing.getJSONObject(i);
                String zutat_name = zutaten.get("name").toString();
                String einheit = zutaten.get("unit").toString();
                i++;
                if (!MainActivity.h_produkt.checkIfProductExists(zutat_name)){
                    int einheit_id = MainActivity.h_einheit.getIdByName(einheit);
                    if (einheit_id == -1)
                        einheit_id = 1;
                    MainActivity.h_produkt.addProdukt(zutat_name, 0, 1, einheit_id);
                }
            }

            i = 0;

            while(i < ing.length()) {
                zutaten = ing.getJSONObject(i);
                String zutat_name = zutaten.get("name").toString();
                String einheit = zutaten.get("unit").toString();
                int menge = 0;

                if (zutaten.getInt("amount") != 0) {
                    menge = Integer.valueOf(zutaten.get("amount").toString());
                }
                i++;

                ar_zutaten.add(new zutat(0, 0, MainActivity.h_produkt.getIdByName(zutat_name), menge, einheit));

            }

            int rezept_id = MainActivity.h_rezept.addRezept(rezept_name, 0, 0, "");
            for (zutat z: ar_zutaten) {
                MainActivity.h_zutat.addZutat(rezept_id, z.getProdukt_id(), z.getMenge(), z.getEinheit());
            }

        }
        catch (Exception e){
            Log.d("IMPORT: ", "FEHLER");
        }
        return "LUL";
    }


}
