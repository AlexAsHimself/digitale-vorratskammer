package j.gruppe.digitalevorratskammer.adapter;

/**
 * @author Alexander Henseler
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import j.gruppe.digitalevorratskammer.R;
import j.gruppe.digitalevorratskammer.activitys.MainActivity;
import j.gruppe.digitalevorratskammer.datamodels.listeneintrag;
import j.gruppe.digitalevorratskammer.datamodels.vorrat;


public class adapter_wish extends ArrayAdapter<listeneintrag> {

    public adapter_wish(Context context, ArrayList<listeneintrag> users) {
        super(context, 0, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        listeneintrag wish = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_larder, parent, false);
        }

        TextView tv_name = convertView.findViewById(R.id.item_title_larder_list);
        TextView tv_menge = convertView.findViewById(R.id.item_quantity_larder_list);
        TextView tv_einheit = convertView.findViewById(R.id.item_volumeUnit_larder_list);
        TextView tv_id = convertView.findViewById(R.id.id_larder_item);
        ImageView iv_icon = convertView.findViewById(R.id.item_categoryIcon_larder_list);
        int produktID = wish.getProdukt_id();

        String einheit = MainActivity.h_einheit.getNameById(MainActivity.h_produkt.getUnitById(produktID));
        String name = MainActivity.h_produkt.getNameById(produktID);
        String menge = String.valueOf(wish.getMenge());

        tv_name.setText(name);
        tv_menge.setText(menge);
        tv_einheit.setText(einheit);

        switch (MainActivity.h_produkt.getCategoryById(wish.getProdukt_id())){
            case 2:
                iv_icon.setImageResource(R.drawable.ic_rice);
                break;
            case 3:
                iv_icon.setImageResource(R.drawable.ic_obst);
                break;
            case 4:
                iv_icon.setImageResource(R.drawable.ic_gemuese);
                break;
            case 5:
                iv_icon.setImageResource(R.drawable.ic_fleisch);
                break;
            case 6:
                iv_icon.setImageResource(R.drawable.ic_suess);
                break;
            case 7:
                iv_icon.setImageResource(R.drawable.ic_drink);
                break;
            case 8:
                iv_icon.setImageResource(R.drawable.ic_shampoo);
                break;
            default:
                iv_icon.setImageResource(R.drawable.ic_sonstiges);
        }

        tv_id.setText(String.valueOf(wish.getId()));
        return convertView;
    }

}
