package j.gruppe.digitalevorratskammer.adapter;

/**
 * @author Alexander Henseler
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import j.gruppe.digitalevorratskammer.R;
import j.gruppe.digitalevorratskammer.datamodels.rezept;

/**
 * Created by AlexHimself on 24.06.2018.
 */

public class adapter_rezepte extends ArrayAdapter<rezept> {

    public adapter_rezepte(Context context, ArrayList<rezept> users) {
        super(context, 0, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        rezept rzpt = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_recipes, parent, false);
        }

        TextView tv_name = convertView.findViewById(R.id.txt_recipeTitle);
        TextView tv_id = convertView.findViewById(R.id.id_recipes_item);

        tv_name.setText(rzpt.getName());
        tv_id.setText(String.valueOf(rzpt.getId()));

        return convertView;
    }
}



