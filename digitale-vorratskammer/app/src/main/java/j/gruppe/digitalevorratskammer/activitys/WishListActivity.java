package j.gruppe.digitalevorratskammer.activitys;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.Spinner;

import java.util.ArrayList;

import j.gruppe.digitalevorratskammer.R;
import j.gruppe.digitalevorratskammer.adapter.adapter_larder;
import j.gruppe.digitalevorratskammer.adapter.adapter_wish;
import j.gruppe.digitalevorratskammer.datamodels.listeneintrag;
import j.gruppe.digitalevorratskammer.datamodels.produkt;
import j.gruppe.digitalevorratskammer.datamodels.vorrat;

/**
 * @author Alexander Henseler
 */

public class WishListActivity  extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private DrawerLayout mDrawerLayout;

    Spinner spinn_sorting;
    ListView list_wish;
    FloatingActionButton bt_addItem;
    Context context;
    private adapter_wish adp_wish;
    private ArrayAdapter<String> adapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist);

        context = this;

        spinn_sorting = findViewById(R.id.spinn_larder);
        list_wish = findViewById(R.id.list_wish);
        bt_addItem = findViewById(R.id.bt_add_stock);

        //Toolbar mit Button für Drawer aktivieren
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        mDrawerLayout = findViewById(R.id.drawer_layout);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_add);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        adp_wish = new adapter_wish(this, MainActivity.h_listeneintrag.ar_listeneintrag_wunsch);
        list_wish.setAdapter(adp_wish);

        fillSortSpinner();

        bt_addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_wish_manual();
            }
        });

        list_wish.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> arg0, View v, int index, long arg3) {
                listeneintrag wish = (listeneintrag) list_wish.getItemAtPosition(index);
                requestAmount(wish.getProdukt_id());
                return true;
            }
        });

        spinn_sorting.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String sort_selection = adapter.getItem(position);
                switch (sort_selection){
                    case "Name - aufsteigend":
                        MainActivity.combine_sort.sort_list("Wunsch", 2);
                        adp_wish.notifyDataSetChanged();
                        break;
                    case "Kategorie - aufsteigend":
                        MainActivity.combine_sort.sort_list("Wunsch", 1);
                        adp_wish.notifyDataSetChanged();
                        break;
                    case "Menge - aufsteigend":
                        MainActivity.combine_sort.sort_list("Wunsch", 3);
                        adp_wish.notifyDataSetChanged();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {}

        });
    }

    private void fillSortSpinner(){
        ArrayList<String> sort_types = new ArrayList<String>();
        sort_types.add("Name - aufsteigend");
        sort_types.add("Kategorie - aufsteigend");
        sort_types.add("Menge - aufsteigend");
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sort_types);
        spinn_sorting.setAdapter(adapter);
    }

    private void add_wish_manual(){
        final Dialog d = new Dialog(context);
        d.setTitle("Hinzufügen");
        d.setContentView(R.layout.alert_add_larder);
        ArrayList<String> all_products = new ArrayList<String>();
        for (produkt prod: MainActivity.h_produkt.ar_produkte){
            all_products.add(prod.getName());
        }
        final Spinner spinner_produkt = d.findViewById(R.id.alert_larder_spinner_produkt);
        ArrayAdapter<String> adapter_produkte = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, all_products);
        adapter_produkte.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_produkt.setAdapter(adapter_produkte);

        final NumberPicker np = d.findViewById(R.id.numberPicker);
        np.setMinValue(1);
        np.setMaxValue(500);
        np.setWrapSelectorWheel(false);
        Button bt_ok = d.findViewById(R.id.bt_ok);

        bt_ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                if (spinner_produkt.getSelectedItemPosition() != -1) {
                    produkt prod = MainActivity.h_produkt.ar_produkte.get(spinner_produkt.getSelectedItemPosition());
                    addAmountOfProdukt(prod.getId(), np.getValue());
                }
                d.dismiss();
            }
        });
        d.show();
    }

    private void addAmountOfProdukt(int _produkt_id, int _amt) {
        MainActivity.h_listeneintrag.addListEntry(_produkt_id, 2, _amt);
        adp_wish.notifyDataSetChanged();
    }

    private void setAmountofProdukt(int _produkt_id, int _amt) {
        MainActivity.h_listeneintrag.setEntryAmount(_produkt_id, _amt, 2);
        adp_wish.notifyDataSetChanged();
    }

    private void requestAmount(final int _produkt_id){
        final Dialog d = new Dialog(context);
        d.setTitle("Menge setzen");
        d.setContentView(R.layout.alert_amount_picker);
        final NumberPicker np = d.findViewById(R.id.numberPicker);
        np.setMinValue(0);
        np.setMaxValue(500);
        np.setWrapSelectorWheel(false);
        Button bt_ok = d.findViewById(R.id.bt_ok);
        bt_ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                setAmountofProdukt(_produkt_id, np.getValue());
                d.dismiss();
            }
        });
        d.show();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_larder) {
            Intent intent = new Intent(this, LarderActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_list) {
            Intent intent = new Intent(this, ShoppingListActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_wishlist) {
            Intent intent = new Intent(this, WishListActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_recipe) {
            Intent intent = new Intent(this, RecipesActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_product){
            Intent intent = new Intent(this, ProductActivity.class);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
