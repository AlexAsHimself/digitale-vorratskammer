package j.gruppe.digitalevorratskammer.handler;

/**
 * @author Alexander Henseler
 */

import android.util.Log;
import java.util.ArrayList;
import j.gruppe.digitalevorratskammer.database.sql_datasource;
import j.gruppe.digitalevorratskammer.datamodels.einheit;

public class handler_einheit {
    public static ArrayList<einheit> ar_einheit = new ArrayList<>();
    public static sql_datasource sql_source;

    public handler_einheit(sql_datasource _sql_source){
        sql_source = _sql_source;
        fillArrayList();
    }

    private void fillArrayList(){
        ar_einheit = sql_source.getUnits();
    }

    public String getNameById(int _id){
        String einh_name = null;
        for (einheit einh: ar_einheit){
            if (einh.getId() == _id){
                einh_name = einh.getName();
            }
        }
        return einh_name;
    }

    public int getIdByName(String name){
        int id = -1;
        for (einheit einh: ar_einheit){
            if (einh.getName().equals(name)) {
                id = einh.getId();
            }
        }
        return id;
    }



}
