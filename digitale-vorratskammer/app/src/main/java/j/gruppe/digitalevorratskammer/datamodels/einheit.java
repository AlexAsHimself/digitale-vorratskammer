package j.gruppe.digitalevorratskammer.datamodels;

/**
 * @author Alexander Henseler
 */

public class einheit {
    private int id;
    private String name;

    public einheit(int _id, String _name){
        setId(_id);
        setName(_name);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
