package j.gruppe.digitalevorratskammer.activitys;

/**
 * @author Alexander Henseler
 */

import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import j.gruppe.digitalevorratskammer.R;
import j.gruppe.digitalevorratskammer.adapter.adapter_zutat_edit;
import j.gruppe.digitalevorratskammer.datamodels.produkt;
import j.gruppe.digitalevorratskammer.datamodels.rezept;
import j.gruppe.digitalevorratskammer.datamodels.zutat;

public class RecipesEditActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    ImageView image_recipe;
    TextView txt_recipeTitle;
    ListView list_ingredients;
    Button bt_save;
    Button bt_addIngredint;
    Button bt_delete;

    private DrawerLayout mDrawerLayout;
    private Uri picture_path;
    private ArrayList<zutat> ar_neue_zutaten = new ArrayList<zutat>();
    private Context context;
    private int id_counter = 0;
    private adapter_zutat_edit adp_zut;
    private boolean edit = false;
    private int id = 0;
    private Bitmap bitmap = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipes_edit);

        image_recipe = findViewById(R.id.image_recipeEdit);
        txt_recipeTitle = findViewById(R.id.txt_title_recipeEdit);
        list_ingredients = findViewById(R.id.list_ingredients_recipeEdit);
        bt_save = findViewById(R.id.bt_save_recipeEdit);
        bt_addIngredint = findViewById(R.id.bt_add_recipeEdit);
        bt_delete = findViewById(R.id.bt_delete_recipeEdit);

        image_recipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                select_picture();
            }
        });

        setTitle("Rezepte erstellen/bearbeiten");
        context = this;

        //Toolbar mit Button für Drawer aktivieren
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        mDrawerLayout = findViewById(R.id.drawer_layout);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_add);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (getIntent().getExtras() != null) {
            Log.d("EDIT: ", "Daten eintragen");
            edit = true;
            Bundle b = getIntent().getExtras();
            id = b.getInt("id");
            rezept rzp = MainActivity.h_rezept.getObjById(id);
            txt_recipeTitle.setText(rzp.getName());
            ar_neue_zutaten = MainActivity.h_zutat.getZutatForRezept(id);
            try {
                String file_name = String.valueOf(txt_recipeTitle.getText().toString().hashCode()).substring(1) + ".jpg";
                ContextWrapper cw = new ContextWrapper(getApplicationContext());
                File directory = cw.getDir(Environment.DIRECTORY_PICTURES, Context.MODE_PRIVATE);
                File mypath = new File(directory, file_name);
                bitmap = BitmapFactory.decodeStream(new FileInputStream(mypath));
                image_recipe.setImageBitmap(bitmap);
            }
            catch (Exception e){}
        }

        adp_zut = new adapter_zutat_edit(this, ar_neue_zutaten);
        list_ingredients.setAdapter(adp_zut);

        bt_addIngredint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_zutat();
            }
        });

        bt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveRecipe();
            }
        });

        bt_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteRecipe(true);
            }
        });
    }

    private void select_picture(){
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 4001);
    }

    private void add_zutat(){
        final Dialog d = new Dialog(context);
        d.setTitle("Zutat hinzufügen");
        d.setContentView(R.layout.alert_add_larder);
        ArrayList<String> all_products = new ArrayList<String>();
        for (produkt prod: MainActivity.h_produkt.ar_produkte){
            all_products.add(prod.getName());
        }
        final Spinner spinner_produkt = d.findViewById(R.id.alert_larder_spinner_produkt);
        ArrayAdapter<String> adapter_produkte = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, all_products);
        adapter_produkte.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_produkt.setAdapter(adapter_produkte);

        final NumberPicker np = d.findViewById(R.id.numberPicker);
        np.setMinValue(1);
        np.setMaxValue(1000);
        np.setWrapSelectorWheel(false);
        Button bt_ok = d.findViewById(R.id.bt_ok);

        bt_ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                if (spinner_produkt.getSelectedItemPosition() != -1) {
                    produkt prod = MainActivity.h_produkt.ar_produkte.get(spinner_produkt.getSelectedItemPosition());
                    ar_neue_zutaten.add(new zutat(id_counter, 1, prod.getId(), np.getValue(), MainActivity.h_einheit.getNameById(prod.getId())));
                    id_counter += 1;
                    adp_zut.notifyDataSetChanged();
                }
                d.dismiss();
            }
        });
        d.show();
    }

    private void saveRecipe(){
        if (edit){
            deleteRecipe(false);
        }
        int rezept_id = MainActivity.h_rezept.addRezept(txt_recipeTitle.getText().toString(), 1, 0, String.valueOf(txt_recipeTitle.getText().toString().hashCode()));
        if (rezept_id != -1) {
            for (zutat zt : ar_neue_zutaten) {
                MainActivity.h_zutat.addZutat(rezept_id, zt.getProdukt_id(), zt.getMenge(), zt.getEinheit());
            }
            if (bitmap != null) {
                try {
                    String file_name = String.valueOf(txt_recipeTitle.getText().toString().hashCode()).substring(1) + ".jpg";
                    ContextWrapper cw = new ContextWrapper(getApplicationContext());
                    File directory = cw.getDir(Environment.DIRECTORY_PICTURES, Context.MODE_PRIVATE);
                    File mypath = new File(directory, file_name);
                    FileOutputStream fos = new FileOutputStream(mypath);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                    fos.flush();
                    fos.close();
                }
                catch (Exception e){
                    Log.d("BILD: ", e.toString());
                }
            }
            Intent intent = new Intent();
            setResult(6101, intent);
            finish();
        }
        else {
            Log.d("REZEPT: ", "Fehler beim anlegen des Rezepts");
        }
    }

    private void deleteRecipe(boolean finish){
        MainActivity.h_zutat.deleteForRecipe(id);
        MainActivity.h_rezept.deleteRecipe(id);
        Intent intent = new Intent();
        if (!edit) {
            setResult(6102, intent);
            finish();
        }
        if (finish){
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 4001 && resultCode == RESULT_OK && null != data) {
            Uri contentURI = data.getData();
            picture_path = contentURI;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                image_recipe.setImageBitmap(bitmap);
            }
            catch (Exception e){
                Toast.makeText(this, "Fehler beim laden des Bildes", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_larder) {
            Intent intent = new Intent(this, LarderActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_list) {
            Intent intent = new Intent(this, ShoppingListActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_wishlist) {
            Intent intent = new Intent(this, WishListActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_recipe) {
            Intent intent = new Intent(this, RecipesActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_product){
            Intent intent = new Intent(this, ProductActivity.class);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
