package j.gruppe.digitalevorratskammer.datamodels;

/**
 * @author Alexander Henseler
 */

import java.util.ArrayList;

import j.gruppe.digitalevorratskammer.activitys.MainActivity;

public class rezept {
    private int id;
    private String name;
    private int personen;
    private boolean favorit;
    private String bild;
    private ArrayList<zutat> zutaten;

    public rezept(int _id, String _name, int _personen, int _favorit, String _bild){
        setId(_id);
        setName(_name);
        setPersonen(_personen);
        setFavorit(_favorit);
        setBild(_bild);
        zutaten = MainActivity.h_zutat.getZutatForRezept(_id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPersonen() {
        return personen;
    }

    public void setPersonen(int personen) {
        this.personen = personen;
    }

    public boolean getFavorit() {
        return favorit;
    }

    public void setFavorit(int favorit) {
        if (favorit == 0)
            this.favorit = false;
        if (favorit == 1)
            this.favorit = true;
    }

    public String getBild() {
        return bild;
    }

    public void setBild(String bild) {
        this.bild = bild;
    }
    public ArrayList<zutat> getZutaten(){
        return zutaten;
    }

}
