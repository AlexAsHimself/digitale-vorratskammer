package j.gruppe.digitalevorratskammer.handler;

import android.util.Log;

import java.util.ArrayList;

import j.gruppe.digitalevorratskammer.database.sql_datasource;
import j.gruppe.digitalevorratskammer.datamodels.vorrat;

/**
 * @author Alexander Henseler
 */

public class handler_vorrat {
    public static ArrayList<vorrat> ar_vorrat = new ArrayList<>();
    public static sql_datasource sql_source;

    public handler_vorrat(sql_datasource _sql_source) {
        sql_source = _sql_source;
        fillArrayList();
    }


    private void fillArrayList() {
        ar_vorrat = sql_source.getVorrat();
    }

    /**
     * Fügt eine Kategorie zu der Liste hinzu:
     * (1) überprüfen ob  Name bereits vorhanden
     * (2) Wenn nicht vorhanden zur SQL-Datenbank hinzufügen und ID zurückbekommen
     * (3) Zur ArrayList hinzufügen
     * (4) ReturnCode zurück geben
     */
    public int addVorrat(int _produkt_id, int _menge) {
        int return_code = 0;
        vorrat doppelter_vorrat = null;
        for (vorrat vor : ar_vorrat) {
            if (vor.getProdukt_id() == _produkt_id) {
                return_code = 484;
                doppelter_vorrat = vor;
                break;
            }
        }
        if (return_code == 0) {
            int new_id = sql_source.addVorrat(_produkt_id, _menge);
            ar_vorrat.add(new vorrat(new_id, _produkt_id, _menge));
        }
        else if (return_code == 484) {
            int new_menge = _menge + doppelter_vorrat.getMenge();
            sql_source.updateVorrat(_produkt_id, new_menge);
            doppelter_vorrat.setMenge(doppelter_vorrat.getMenge() + _menge);
        }
        return return_code;
    }

    public void removeVorrat(int _produkt_id, int _menge) {
        vorrat old_vor = null;
        for (vorrat vor : ar_vorrat) {
            if (vor.getProdukt_id() == _produkt_id) {
                old_vor = vor;
            }
        }
        old_vor.setMenge(old_vor.getMenge() - _menge);
        sql_source.updateVorrat(_produkt_id, _menge);
        if (old_vor.getMenge() <= 0){
            ar_vorrat.remove(old_vor);
            sql_source.deleteVorrat(_produkt_id);
        }

    }
}
