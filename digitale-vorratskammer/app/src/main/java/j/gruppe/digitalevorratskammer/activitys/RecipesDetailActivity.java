package j.gruppe.digitalevorratskammer.activitys;

/**
 * @author Alexander Henseler
 */

import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

import j.gruppe.digitalevorratskammer.R;
import j.gruppe.digitalevorratskammer.adapter.adapter_zutat_edit;
import j.gruppe.digitalevorratskammer.datamodels.rezept;
import j.gruppe.digitalevorratskammer.datamodels.zutat;

public class RecipesDetailActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    TextView txt_recipeTitle;
    ImageView image_recipe;
    ListView list_ingredients;
    Button bt_addtoshopping;
    Context context;
    int id;
    private DrawerLayout mDrawerLayout;
    private ArrayList<zutat> ar_zutaten;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipes_detail);

        context = this;

        txt_recipeTitle = findViewById(R.id.txt_title_recipe);
        image_recipe = findViewById(R.id.image_recipe);
        list_ingredients = findViewById(R.id.list_recipe);
        bt_addtoshopping = findViewById(R.id.bt_add_ingredients_recipe);
        setTitle("Rezept");

        //Toolbar mit Button für Drawer aktivieren
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        mDrawerLayout = findViewById(R.id.drawer_layout);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_add);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (getIntent().getExtras() != null) {
            Bundle b = getIntent().getExtras();
            id = b.getInt("id");
            rezept rzp = MainActivity.h_rezept.getObjById(id);
            txt_recipeTitle.setText(rzp.getName());
            try {
                String file_name = String.valueOf(txt_recipeTitle.getText().toString().hashCode()).substring(1) + ".jpg";
                ContextWrapper cw = new ContextWrapper(getApplicationContext());
                File directory = cw.getDir(Environment.DIRECTORY_PICTURES, Context.MODE_PRIVATE);
                File mypath = new File(directory, file_name);
                Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(mypath));
                image_recipe.setImageBitmap(bitmap);
            }
            catch (Exception e){}
        }

        ar_zutaten = MainActivity.h_zutat.getZutatForRezept(id);
        adapter_zutat_edit adp_zut = new adapter_zutat_edit(this, ar_zutaten);
        list_ingredients.setAdapter(adp_zut);

        bt_addtoshopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setToShoppingList();
            }
        });
    }

    public void setToShoppingList(){
        for (zutat z: ar_zutaten){
            MainActivity.h_listeneintrag.addListEntry(z.getProdukt_id(), 1, z.getMenge());
        }
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_larder) {
            Intent intent = new Intent(this, LarderActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_list) {
            Intent intent = new Intent(this, ShoppingListActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_wishlist) {
            Intent intent = new Intent(this, WishListActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_recipe) {
            Intent intent = new Intent(this, RecipesActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_product){
            Intent intent = new Intent(this, ProductActivity.class);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

}
