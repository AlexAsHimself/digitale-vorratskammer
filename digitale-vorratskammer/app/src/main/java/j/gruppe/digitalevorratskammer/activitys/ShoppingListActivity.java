package j.gruppe.digitalevorratskammer.activitys;

/**
 * @author Alexander Henseler
 */

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.Spinner;

import java.util.ArrayList;

import j.gruppe.digitalevorratskammer.R;
import j.gruppe.digitalevorratskammer.adapter.adapter_shopping;
import j.gruppe.digitalevorratskammer.datamodels.listeneintrag;
import j.gruppe.digitalevorratskammer.datamodels.produkt;
import j.gruppe.digitalevorratskammer.datamodels.vorrat;

public class ShoppingListActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    Spinner sorting;
    ListView list_shopping;
    FloatingActionButton bt_addItem;
    Context context;
    private DrawerLayout mDrawerLayout;
    adapter_shopping adp_entry;
    private ArrayAdapter<String> adapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shoppinglist);

        context = this;

        sorting = findViewById(R.id.spinn_shoppingList);
        list_shopping = findViewById(R.id.list_shoppingList);
        bt_addItem = findViewById(R.id.bt_add_shopingList);

        setTitle("Einkaufsliste");

        //Toolbar mit Button für Drawer aktivieren
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        mDrawerLayout = findViewById(R.id.drawer_layout);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_add);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        adp_entry = new adapter_shopping(this, MainActivity.h_listeneintrag.ar_listeneintrag_einkauf);
        list_shopping.setAdapter(adp_entry);

        bt_addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add_shoppingListItem();
            }
        });

        fillSortSpinner();

        list_shopping.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int index, long id) {
                listeneintrag entry = (listeneintrag) list_shopping.getItemAtPosition(index);
                MainActivity.h_listeneintrag.checkEntry(entry.getId());
                adp_entry.notifyDataSetChanged();
            }
        });

        sorting.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String sort_selection = adapter.getItem(position);
                switch (sort_selection){
                    case "Name - aufsteigend":
                        MainActivity.combine_sort.sort_list("Einkauf", 2);
                        adp_entry.notifyDataSetChanged();
                        break;
                    case "Kategorie - aufsteigend":
                        MainActivity.combine_sort.sort_list("Einkauf", 1);
                        adp_entry.notifyDataSetChanged();
                        break;
                    case "erledigt":
                        MainActivity.combine_sort.sort_list("Einkauf", 4);
                        adp_entry.notifyDataSetChanged();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {}
        });
    }

    private void fillSortSpinner(){
        ArrayList<String> sort_types = new ArrayList<String>();
        sort_types.add("Name - aufsteigend");
        sort_types.add("Kategorie - aufsteigend");
        sort_types.add("erledigt");
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sort_types);
        sorting.setAdapter(adapter);
    }

    private void add_shoppingListItem() {
        final Dialog d = new Dialog(context);
        d.setTitle("Hinzufügen");
        d.setContentView(R.layout.alert_add_larder);
        ArrayList<String> all_products = new ArrayList<String>();
        for (produkt prod : MainActivity.h_produkt.ar_produkte) {
            all_products.add(prod.getName());
        }
        final Spinner spinner_produkt = d.findViewById(R.id.alert_larder_spinner_produkt);
        ArrayAdapter<String> adapter_produkte = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, all_products);
        adapter_produkte.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_produkt.setAdapter(adapter_produkte);

        final NumberPicker np = d.findViewById(R.id.numberPicker);
        np.setMinValue(1);
        np.setMaxValue(50);
        np.setWrapSelectorWheel(false);
        Button bt_ok = d.findViewById(R.id.bt_ok);

        bt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spinner_produkt.getSelectedItemPosition() != -1) {
                    produkt prod = MainActivity.h_produkt.ar_produkte.get(spinner_produkt.getSelectedItemPosition());
                    final int id = prod.getId();
                    addAmountofProdukt(id, np.getValue());
                }
                d.dismiss();
            }
        });
        d.show();
    }

    private void addAmountofProdukt(int _id, int _amt) {
        if (_amt != 0) {
            MainActivity.h_listeneintrag.addListEntry(_id, 1, _amt);
            adp_entry.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.generate:
                MainActivity.combine_sort.combine();
                adp_entry.notifyDataSetChanged();
                return true;
            case R.id.clear:
                MainActivity.h_listeneintrag.clearShoppingList();
                adp_entry.notifyDataSetChanged();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_larder) {
            Intent intent = new Intent(this, LarderActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_list) {
            Intent intent = new Intent(this, ShoppingListActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_wishlist) {
            Intent intent = new Intent(this, WishListActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_recipe) {
            Intent intent = new Intent(this, RecipesActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_product){
            Intent intent = new Intent(this, ProductActivity.class);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
