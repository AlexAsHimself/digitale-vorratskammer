package j.gruppe.digitalevorratskammer.datamodels;

import java.util.ArrayList;

/**
 * @author Alexander Henseler
 */

public class zutat {
    private int id;
    private int rezept_id;
    private int produkt_id;
    private int menge;
    private String einheit;

    public zutat(int _id, int _rezept_id, int _produkt_id, int _menge, String _einheit){
        setId(_id);
        setRezept_id(_rezept_id);
        setProdukt_id(_produkt_id);
        setMenge(_menge);
        setEinheit(_einheit);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRezept_id() {
        return rezept_id;
    }

    public void setRezept_id(int rezept_id) {
        this.rezept_id = rezept_id;
    }

    public int getProdukt_id() {
        return produkt_id;
    }

    public void setProdukt_id(int produkt_id) {
        this.produkt_id = produkt_id;
    }

    public int getMenge() {
        return menge;
    }

    public void setMenge(int menge) {
        this.menge = menge;
    }

    public String getEinheit() {
        return einheit;
    }

    public void setEinheit(String einheit_id) {
        this.einheit = einheit_id;
    }

}
