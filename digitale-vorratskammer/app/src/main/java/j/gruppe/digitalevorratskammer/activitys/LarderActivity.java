package j.gruppe.digitalevorratskammer.activitys;

/**
 * @author Alexander Henseler
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.Spinner;

import java.util.ArrayList;

import j.gruppe.digitalevorratskammer.R;
import j.gruppe.digitalevorratskammer.adapter.adapter_larder;
import j.gruppe.digitalevorratskammer.datamodels.produkt;
import j.gruppe.digitalevorratskammer.datamodels.vorrat;

public class LarderActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    Spinner spinn_sorting;
    ListView list_stocks;
    FloatingActionButton bt_addItem;
    FloatingActionButton bt_addItem_barcode;
    Context context;
    private DrawerLayout mDrawerLayout;
    private adapter_larder adp_larder;
    private ArrayAdapter<String> adapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_larder);

        context = this;

        spinn_sorting = findViewById(R.id.spinn_larder);
        list_stocks = findViewById(R.id.list_larder);
        bt_addItem = findViewById(R.id.bt_add_stock);
        bt_addItem_barcode = findViewById(R.id.bt_add_stock_barcode);

        setTitle("Vorräte");
        //Toolbar mit Button für Drawer aktivieren
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        mDrawerLayout = findViewById(R.id.drawer_layout);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_add);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        adp_larder = new adapter_larder(this, MainActivity.h_vorrat.ar_vorrat);
        list_stocks.setAdapter(adp_larder);

        fillSortSpinner();

        bt_addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_larder_manual();
            }
        });

        bt_addItem_barcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ScannerActivity.class);
                startActivityForResult(intent, 10);
            }
        });

        list_stocks.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> arg0, View v, int index, long arg3) {
                vorrat vor = (vorrat) list_stocks.getItemAtPosition(index);
                removeAmount(vor);
                return true;
            }
        });

        spinn_sorting.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String sort_selection = adapter.getItem(position);
                switch (sort_selection){
                    case "Name - aufsteigend":
                        MainActivity.combine_sort.sort_list("Vorrat", 2);
                        adp_larder.notifyDataSetChanged();
                        break;
                    case "Kategorie - aufsteigend":
                        MainActivity.combine_sort.sort_list("Vorrat", 1);
                        adp_larder.notifyDataSetChanged();
                        break;
                    case "Menge - aufsteigend":
                        MainActivity.combine_sort.sort_list("Vorrat", 3);
                        adp_larder.notifyDataSetChanged();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {}

        });


    }

    private void fillSortSpinner(){
        ArrayList<String> sort_types = new ArrayList<String>();
        sort_types.add("Name - aufsteigend");
        sort_types.add("Kategorie - aufsteigend");
        sort_types.add("Menge - aufsteigend");
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sort_types);
        spinn_sorting.setAdapter(adapter);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 10) {
            if(resultCode == Activity.RESULT_OK){
                String barcode = data.getStringExtra("barcode");
                if (!barcode.equals("Barcode?")) {
                    if(MainActivity.h_produkt.checkIfBarcodeExists(barcode)) {
                        requestAmount(barcode);
                    }
                    else {
                        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);
                        alertDialogBuilder.setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which){
                                Intent intent = new Intent(getApplicationContext(), ProductDetailActivity.class);
                                startActivityForResult(intent, 3001);
                            }
                        });
                        alertDialogBuilder.setNegativeButton("Nein", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which){

                            }
                        });
                        alertDialogBuilder.setTitle("Nicht Vorhanden");
                        alertDialogBuilder.setMessage("Das Produkt wurde nicht gefunden. Wollen sie es Hinzufügen");
                        alertDialogBuilder.show();
                    }
                }
            }
        }
        else if (requestCode == 3001) {
            if(resultCode == 2102){
                ProductActivity.addProduct(data);
            }
        }
    }

    private void removeAmount(final vorrat vor){
        final Dialog d = new Dialog(context);
        d.setTitle("Vorrat entfernen oder Setzen");
        d.setContentView(R.layout.alert_remove_larder);
        final NumberPicker np = d.findViewById(R.id.numberPicker_remove);
        np.setMinValue(0);
        np.setMaxValue(vor.getMenge());
        np.setWrapSelectorWheel(false);
        Button bt_ok_remove = d.findViewById(R.id.bt_ok_remove);
        Button bt_ok_set = d.findViewById(R.id.bt_ok_set);

        bt_ok_remove.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                int menge = np.getValue();
                if (menge > vor.getMenge()){
                    menge = vor.getMenge();
                }
                removeAmoutofProdukt(vor, menge);
                d.dismiss();
            }
        });

        bt_ok_set.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                int menge = vor.getMenge() - np.getValue();
                if (menge < 0)
                    menge = 0;
                removeAmoutofProdukt(vor, menge);
                d.dismiss();
            }
        });
        d.show();
    }




    private void requestAmount(String _barcode){
        final Dialog d = new Dialog(context);
        d.setTitle("Menge");
        d.setContentView(R.layout.alert_amount_picker);
        final NumberPicker np = d.findViewById(R.id.numberPicker);
        np.setMinValue(1);
        np.setMaxValue(50);
        np.setWrapSelectorWheel(false);
        Button bt_ok = d.findViewById(R.id.bt_ok);
        final String bar = _barcode;
        bt_ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                int prod_id = MainActivity.h_produkt.getIdByBarcode(bar);
                addAmountofProdukt(prod_id, np.getValue());
                d.dismiss();
            }
        });
        d.show();
    }

    private void add_larder_manual(){
        final Dialog d = new Dialog(context);
        d.setTitle("Hinzufügen");
        d.setContentView(R.layout.alert_add_larder);
        ArrayList<String> all_products = new ArrayList<String>();
        for (produkt prod: MainActivity.h_produkt.ar_produkte){
            all_products.add(prod.getName());
        }
        final Spinner spinner_produkt = d.findViewById(R.id.alert_larder_spinner_produkt);
        ArrayAdapter<String> adapter_produkte = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, all_products);
        adapter_produkte.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_produkt.setAdapter(adapter_produkte);

        final NumberPicker np = d.findViewById(R.id.numberPicker);
        np.setMinValue(1);
        np.setMaxValue(50);
        np.setWrapSelectorWheel(false);
        Button bt_ok = d.findViewById(R.id.bt_ok);

        bt_ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                if (spinner_produkt.getSelectedItemPosition() != -1) {
                    produkt prod = MainActivity.h_produkt.ar_produkte.get(spinner_produkt.getSelectedItemPosition());
                    final int id = prod.getId();
                    addAmountofProdukt(id, np.getValue());
                }
                d.dismiss();
            }
        });
        d.show();
    }

    private void addAmountofProdukt(int _id, int _amt) {
        if (_amt != 0) {
            MainActivity.h_vorrat.addVorrat(_id, _amt);
            adp_larder.notifyDataSetChanged();
        }
    }

    private void removeAmoutofProdukt(vorrat vor, int menge){
        MainActivity.h_vorrat.removeVorrat(vor.getProdukt_id(), menge);
        adp_larder.notifyDataSetChanged();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_larder) {
            Intent intent = new Intent(this, LarderActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_list) {
            Intent intent = new Intent(this, ShoppingListActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_wishlist) {
            Intent intent = new Intent(this, WishListActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_recipe) {
            Intent intent = new Intent(this, RecipesActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_product){
            Intent intent = new Intent(this, ProductActivity.class);
            startActivity(intent);
            finish();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
